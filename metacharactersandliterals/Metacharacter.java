package metacharactersandliterals;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * Класс литералы и метасимволы
 *
 * @author Tropanova N.S.
 */
public class Metacharacter {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("[\\s]");

        Matcher matcher1 = pattern.matcher("a b c d e f g h");
        System.out.println(matcher1.find());

        Matcher matcher2 = pattern.matcher("f g h a b c ");
        System.out.println(matcher2.find());

        Matcher matcher3 = pattern.matcher("1");
        System.out.println(matcher3.find());
    }
}
