package grouping;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс группировка регулярных выражений
 *
 * @author Tropanova N.S.
 */
public class groupingRegex {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("(\\d+)");
        Matcher matcher = pattern.matcher("Java 7 Java 7");

        while (matcher.find()) {
            System.out.println(matcher.group());


        }
    }
}
