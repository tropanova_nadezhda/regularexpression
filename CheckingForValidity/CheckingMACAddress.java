package CheckingForValidity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс проверка MAC-адреса на валидность
 *
 * @author Tropanova N.S.
 */
public class CheckingMACAddress {
    public static void main(String[] args) {

        int counter = 0;
        String MAC = "77-a3-d2-01-ff-63";
        Pattern pattern = Pattern.compile("^((\\p{XDigit}{2}([:-]|$)){6})$");

        Matcher matcher = pattern.matcher(MAC);

        while (matcher.find()) {
            counter++;
            System.out.println("Найдено совпадение '" +
                    MAC.substring(matcher.start(), matcher.end()) +
                    "', начиная с индекса " + matcher.start() +
                    " и строка в индексе " + matcher.end());
        }
        System.out.println("Найденные совпадения: " + counter);
    }
}

