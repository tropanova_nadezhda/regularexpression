package CheckingForValidity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс проверка IPv4 на валидность
 *
 * @author Tropanova N.S.
 */
public class CheckingIPv4 {
    public static void main(String[] args) {

        int counter = 0;
        String IPv4 = "0.0.0.0";
        Pattern pattern = Pattern.compile("^(((\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.){3}(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5]))$");
        Matcher matcher = pattern.matcher(IPv4);

        while (matcher.find()) {
            counter++;
            System.out.println("Найдено совпадение '" +
                    IPv4.substring(matcher.start(), matcher.end()) +
                    "', начиная с индекса " + matcher.start() +
                    " и строка в индексе " + matcher.end());
        }
        System.out.println("Найденные совпадения: " + counter);
    }
}
