package CheckingForValidity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс проверка IPv6 на валидность
 *
 * @author Tropanova N.S.
 */
public class CheckingIPv6 {
    public static void main(String[] args) {

        int counter = 0;
        String IPv6 = "1:1:1:1:1:1::1";
        Pattern pattern = Pattern.compile("((^|:)([0-9a-fA-F]{0,4})){1,8}$");

        Matcher matcher = pattern.matcher(IPv6);

        while (matcher.find()) {
            counter++;
            System.out.println("Найдено совпадение '" +
                    IPv6.substring(matcher.start(), matcher.end()) +
                    "', начиная с индекса " + matcher.start() +
                    " и строка в индексе " + matcher.end());
        }
        System.out.println("Найденные совпадения: " + counter);
    }

}
