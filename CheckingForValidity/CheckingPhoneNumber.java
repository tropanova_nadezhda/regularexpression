package CheckingForValidity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс проверка номера телефона на валидность
 *
 * @author Tropanova N.S.
 */
public class CheckingPhoneNumber {
    public static void main(String[] args) {


        String phoneNumber = "+79875024783";
        Pattern pattern = Pattern.compile("^((\\+?798)([0-9]{9}))$");
        Matcher matcher = pattern.matcher(phoneNumber);

        if (matcher.matches()) {

            System.out.println("Hомер телефона " + phoneNumber + " правильный");
        } else {
            System.out.println("Hомер телефона " + phoneNumber + " неверный");
        }
    }
}