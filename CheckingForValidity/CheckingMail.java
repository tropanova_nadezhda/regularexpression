package CheckingForValidity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс проверка e-mail на валидность
 *
 * @author Tropanova N.S.
 */
public class CheckingMail {
    public static void main(String[] args) {

        int counter = 0;
        String email = "example@gmail.com";
        Pattern pattern = Pattern.compile("^((\\w|[-+])+(\\.[w-]+)*@[\\w-]+((\\.[\\d\\p{Alpha}]+)*(\\.\\p{Alpha}{2,})*)*)$");

        Matcher matcher = pattern.matcher(email);

        while (matcher.find()) {
            counter++;
            System.out.println("Найдено совпадение '" +
                    email.substring(matcher.start(), matcher.end()) +
                    "', начиная с индекса " + matcher.start() +
                    " и строка в индексе " + matcher.end());
        }
        System.out.println("Найденные совпадения: " + counter);
    }
}
