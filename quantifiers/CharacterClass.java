package quantifiers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * Класс символьный
 *
 * @author Tropanova N.S.
 */
public class CharacterClass {
    public static void main(String[] args) {

        int counter = 0;
        String string = "abod1234";
        Pattern pattern = Pattern.compile("\\p{Digit}");
        Matcher matcher = pattern.matcher(string);

        while (matcher.find()) {
            counter++;
            System.out.println("Найдено совпадение " + string.substring(matcher.start(),matcher.end())+
                    ", начиная с индекса " + matcher.start()+
                    " и строка в индексе " + matcher.end());
        }
        System.out.println("Найденные совпадения " + counter);
    }
}
