package quantifiers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс ревнивая (сверхжадная) квантификация
 *
 * @author Tropanova N.S.
 */
public class JealousQuantification {
    public static void main(String[] args) {

        int counter = 0;
        String string = "198.134.196.1.14519";
        Pattern pattern = Pattern.compile(".*+19");
        Matcher matcher = pattern.matcher(string);

        while (matcher.find()) {
            counter++;
            System.out.println("Найдено совпадение " + string.substring(matcher.start(),matcher.end())+
                    ", начиная с индекса " + matcher.start()+
                    " и строка в индексе " + matcher.end());
        }
        System.out.println("Найденные совпадения " + counter);
    }
}
